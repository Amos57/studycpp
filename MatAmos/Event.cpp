#include <Windows.h>
#include "com_amos_event_Event.h"



/*
 * Class:     com_amos_event_Event
 * Method:    createEvent
 * Signature: (JZZLjava/lang/String;)Lcom/amos/event/Event;
 */
JNIEXPORT jobject JNICALL Java_com_amos_event_Event_createEvent__JZZLjava_lang_String_2
(JNIEnv * env, jclass  cls, jlong pa, jboolean b1, jboolean b2, jstring name) {
	jclass exClass = env->FindClass("com/amos/event/Event");
	jmethodID constructorEvent = env->GetMethodID(exClass, "<init>", "()V");
	jobject creatEvent=env->NewObject(exClass, constructorEvent);
	jclass clazz = env->GetObjectClass(creatEvent);

	jfieldID pointerEvent = env->GetFieldID(clazz, "pointerEvent", "J");

	    //NULL,               // default security attributes
	const bool & mre =  b1;	//TRUE,               // manual-reset event
	const bool & isin = b2;	//FALSE,              // initial state is nonsignaled
	const jchar* jcstr = env->GetStringChars(name, 0);	//TEXT("WriteEvent")  // object name
	LPSECURITY_ATTRIBUTES la = NULL;

	HANDLE event = CreateEvent(la,mre, isin, (LPCWSTR)jcstr);

	if (event == INVALID_HANDLE_VALUE) {
		return nullptr;
	}
	else
	{
		env->SetLongField(creatEvent, pointerEvent, reinterpret_cast<jlong>( event));
	}
	return creatEvent;

}

/*
 * Class:     com_amos_event_Event
 * Method:    createEvent
 * Signature: (JZZ)Lcom/amos/event/Event;
 */
JNIEXPORT jobject JNICALL Java_com_amos_event_Event_createEvent__JZZ
(JNIEnv * env, jclass cls, jlong l, jboolean b1, jboolean b2) {
	return Java_com_amos_event_Event_createEvent__JZZLjava_lang_String_2(env, cls, l, b1, b2, NULL);
}



