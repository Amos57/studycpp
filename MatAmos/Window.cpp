#include <Windows.h>
#include "com_amos_event_gui_Window.h"
#include <vector>
#include <iostream>


struct ComponentListener{

	int componentHWND;
	HMENU idListener;
	jobject javaListener;

};
JNIEnv * jniEnv;
std::vector<ComponentListener> listenComponents;


/*
 * Class:     com_amos_event_gui_Window
 * Method:    startup
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_com_amos_event_gui_Window_startup
(JNIEnv * env, jobject o) {
	jniEnv = env;
	std::cout << "start" << std::endl;

	jclass classWindow = env->GetObjectClass(o);
	jfieldID width = env->GetFieldID(classWindow,"width","I");
	jfieldID height = env->GetFieldID(classWindow, "height", "I");
	jfieldID name = env->GetFieldID(classWindow, "title", "Ljava/lang/String;");

	jfieldID descriptorWindow = env->GetFieldID(classWindow, "pointerEvent", "J");


	int wd = env->GetIntField(o,width);


	int h = env->GetIntField(o, height);

	jstring ttl = (jstring)env->GetObjectField(o,name);

	const jchar * title = env->GetStringChars(ttl,0);



	HINSTANCE istanc = GetModuleHandle(NULL);

	std::cout << istanc << std::endl;

	HWND hwnd; // ���������� ����
	MSG msg; // ��������� ���������
	WNDCLASS w; // ��������� ������ ����
	// ����������� ������ ����
	memset(&w, 0, sizeof(WNDCLASS));
	w.style = CS_HREDRAW | CS_VREDRAW;
	w.hInstance = istanc;
	w.hbrBackground = (HBRUSH)(WHITE_BRUSH);
	w.lpszClassName = L"My Class";


	w.lpfnWndProc = [](HWND wind, UINT keyMassage,WPARAM wParam, LPARAM lParam)->LRESULT {
	
	     
		switch (keyMassage)
		{
		case WM_DESTROY: {
			PostQuitMessage(0);
			std::cout << "exit from programm" << std::endl;


		}
						 return 0;
		case WM_COMMAND: 
		{
		
			for (int i = 0; i < listenComponents.size(); i++) {
				if (wParam == reinterpret_cast<WPARAM>( listenComponents[i].idListener)) {
					jclass cls = jniEnv->GetObjectClass(listenComponents[i].javaListener);
					jmethodID actionPerformed = jniEnv->GetMethodID(cls,"actionPerformed", "()V");
					jniEnv->CallVoidMethod(listenComponents[i].javaListener,actionPerformed);
				}
			
			}
		
		
		
		}
						 return 0;
		default:
			return DefWindowProc(wind, keyMassage, wParam, lParam);
		}
	};

	RegisterClass(&w);
	// �������� ����

	hwnd = CreateWindow(TEXT("My Class"), (LPCWSTR)title, WS_OVERLAPPEDWINDOW, 500, 300, wd, h, NULL, NULL, istanc, NULL);
	env->SetLongField(classWindow, descriptorWindow, (jlong)hwnd);
	
	ShowWindow(hwnd, 1); // �����������
	UpdateWindow(hwnd);          // �����������


	// ���� ��������� ���������
	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);

	}

}

/*
 * Class:     com_amos_event_gui_Window
 * Method:    update
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_com_amos_event_gui_Window_update
(JNIEnv *, jobject) {

}

/*
 * Class:     com_amos_event_gui_Window
 * Method:    windowListener
 * Signature: (Lcom/amos/event/gui/WindowEvent;)V
 */
JNIEXPORT void JNICALL Java_com_amos_event_gui_Window_windowListener
(JNIEnv *, jobject, jobject) {

}