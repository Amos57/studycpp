#include <Windows.h>
#include "com_amos_matrix_gui_GuiUtil.h"


/*
 * Class:     com_amos_matrix_gui_GuiUtil
 * Method:    messageBox
 * Signature: (Ljava/lang/String;Ljava/lang/String;J)I
 */
JNIEXPORT jint JNICALL Java_com_amos_matrix_gui_GuiUtil_messageBox
(JNIEnv * env, jclass, jstring msg, jstring title, jlong params) {


	const jchar* jcstr = env->GetStringChars(msg, 0);
	const jchar* titl = env->GetStringChars(title, 0);
	HANDLE h;

	UINT param = (UINT)params;
	LPCWSTR m=(LPCWSTR) jcstr;
	LPCWSTR t= (LPCWSTR)titl;
	return (jint)MessageBox(NULL,m,t,params);

}

