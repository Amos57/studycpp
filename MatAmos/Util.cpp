#include <Windows.h>
#include "com_amos_event_Util.h"


/*
 * Class:     com_amos_event_Util
 * Method:    closeHandle
 * Signature: (Lcom/amos/event/Handle;)V
 */
JNIEXPORT void JNICALL Java_com_amos_event_Util_closeHandle
(JNIEnv * env, jclass, jobject o) {

	jclass ClassMYView = env->GetObjectClass(o);
	jfieldID pointerEvent = env->GetFieldID(ClassMYView, "pointerEvent", "J");
	int point = env->GetIntField(o, pointerEvent);
	if (point != 0) {
		HANDLE h = reinterpret_cast<HANDLE>(point);
		CloseHandle(h);
	}
	

}

/*
 * Class:     com_amos_event_Util
 * Method:    WaitForSingleObject
 * Signature: (Lcom/amos/event/Event;J)J
 */
JNIEXPORT jlong JNICALL Java_com_amos_event_Util_WaitForSingleObject
(JNIEnv *env, jclass this_, jobject o, jlong mls) {
	jclass ClassMYView = env->GetObjectClass(o);
	jfieldID pointerEvent = env->GetFieldID(ClassMYView, "pointerEvent", "J");
	int point = env->GetIntField(o, pointerEvent);
	if (point != 0) {
		HANDLE h = reinterpret_cast<HANDLE>(point);
		return WaitForSingleObject(h,mls);
	}
	else return 0;
}

/*
 * Class:     com_amos_event_Util
 * Method:    postMessage
 * Signature: (Ljava/lang/Object;I)Z
 */
JNIEXPORT jboolean JNICALL Java_com_amos_event_Util_postMessage
(JNIEnv *, jclass, jobject, jint) {
	return false;
}

/*
 * Class:     com_amos_event_Util
 * Method:    getAsyncKeyState
 * Signature: (I)S
 */
JNIEXPORT jshort JNICALL Java_com_amos_event_Util_getAsyncKeyState
(JNIEnv *, jclass, jint numKey) {
	return GetAsyncKeyState(numKey);
}

/*
 * Class:     com_amos_event_Util
 * Method:    getAKeyState
 * Signature: (I)S
 */
JNIEXPORT jshort JNICALL Java_com_amos_event_Util_getAKeyState
(JNIEnv *, jclass, jint numKey) {
	return GetKeyState(numKey);
}

/*
 * Class:     com_amos_event_Util
 * Method:    setEvent
 * Signature: (Lcom/amos/event/Event;)V
 */
JNIEXPORT void JNICALL Java_com_amos_event_Util_setEvent
(JNIEnv * env, jclass, jobject o) {
	jclass ClassMYView = env->GetObjectClass(o);
	jfieldID pointerEvent = env->GetFieldID(ClassMYView, "pointerEvent", "J");
	int point = env->GetIntField(o, pointerEvent);
	if (point != 0) {
		HANDLE h = reinterpret_cast<HANDLE>(point);
		SetEvent(h);
	}

}


/*
 * Class:     com_amos_event_Util
 * Method:    ResetEvent
 * Signature: (Lcom/amos/event/Event;)Z
 */
JNIEXPORT jboolean JNICALL Java_com_amos_event_Util_ResetEvent
(JNIEnv *env, jclass, jobject o) {
	jclass ClassMYView = env->GetObjectClass(o);
	jfieldID pointerEvent = env->GetFieldID(ClassMYView, "pointerEvent", "J");
	int point = env->GetIntField(o, pointerEvent);
	if (point != 0) {
		HANDLE h = reinterpret_cast<HANDLE>(point);
		return ResetEvent(h);
	}
	return false;

}